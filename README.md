# AFP - Money Tracker

Elm web app created as a term project of Applied Functional Programming course at CTU, Prague.

## Introduction

The intention is to create a simple, free and open-source application to manage person's money.

## Todos

- [ ] Adding, modifying and deleting records.
    - [ ] Currencies
        - [ ] Posibility to add a record in any recognized currency.
        - [ ] Ability to chose and change your main currency.
    - [ ] Categories
        - [ ] All categories already used will be visible.
        - [ ] Ability to rename a category in all records.
    - [ ] Marketable value (0 by default).
        * e.g. when buying a table for 100USD, an assumption it can be sold for 50USD.
    - [ ] Date of transaction (by default the input date).
    - [ ] Note (optional).
- [ ] CSV export and import
    - [ ] Ability to export records based on filters.
    - [ ] Ability to import records (duplicates will be ignored).
- [ ] Visualization
    - [ ] Money inflows, outflows, balance.
        - [ ] Bar charts for categories, currencies.
            - Maybe pie charts, depending on what will look better.
        - [ ] Line charts for changes in time.
            - [ ] Ability to filter categories, currencies.

## Don'ts

- Simple UI, no animations, no transitions etc.
- One user only, no authentication.
- One language, the app will be in English.
